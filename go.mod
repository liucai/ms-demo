module cossbow.com/ms

go 1.13

require (
	github.com/TarsCloud/TarsGo v0.0.0-20200121082531-da86cb174e6b
	github.com/coreos/etcd v3.3.17+incompatible
	github.com/golang/protobuf v1.3.2
	github.com/hashicorp/consul/api v1.3.0
	github.com/micro/go-micro v1.18.0 // indirect
	github.com/wothing/wonaming v0.0.0-20180810082955-718c29fa5918
	go.etcd.io/etcd v0.0.0-00010101000000-000000000000
	google.golang.org/grpc v1.26.0
)

replace (
	go.etcd.io/etcd => github.com/etcd-io/etcd v3.3.18+incompatible
	golang.org/x/crypto => github.com/golang/crypto v0.0.0-20200117160349-530e935923ad
	golang.org/x/mod => github.com/golang/mod v0.2.0
	golang.org/x/net => github.com/golang/net v0.0.0-20200114155413-6afb5195e5aa
	golang.org/x/sys => github.com/golang/sys v0.0.0-20200124204421-9fbb57f87de9
	golang.org/x/text => github.com/golang/text v0.3.0
	golang.org/x/time => github.com/golang/time v0.0.0-20191024005414-555d28b269f0
	golang.org/x/tools => github.com/golang/tools v0.0.0-20200125223703-d33eef8e6825
	golang.org/x/xerrors => github.com/golang/xerrors v0.0.0-20191204190536-9bdfabe68543
	google.golang.org/genproto => gitee.com/mirrors/go-genproto v0.0.0-20200122232147-0452cf42e150
	google.golang.org/grpc => github.com/grpc/grpc-go v1.26.0
)
