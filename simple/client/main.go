package main

import (
	"context"
	"cossbow.com/ms/etcdv3"
	"cossbow.com/ms/rpc"
	. "cossbow.com/ms/util"
	"fmt"

	"google.golang.org/grpc"
	"google.golang.org/grpc/resolver"
	"time"
)

func main() {
	r := etcdv3.NewResolver("127.0.0.1:2379")
	resolver.Register(r)

	con, er := grpc.Dial(r.Scheme()+"://author/test/hello", grpc.WithInsecure(), grpc.WithBalancerName("round_robin"))
	ThrowException(er)
	client := rpc.NewGreeterClient(con)

	for i := 0; i < 10000; i++ {
		resp, er := client.SayHello(context.Background(), &rpc.HelloRequest{Name: "蒋建军"})
		ThrowException(er)
		fmt.Println(resp.Message)
		time.Sleep(time.Millisecond * 200)
	}
}
