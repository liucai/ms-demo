// impl
package rpc

import (
	"context"
)

type GreeterServerImpl struct {
	Name string
}

func (s *GreeterServerImpl) SayHello(ctx context.Context, req *HelloRequest) (*HelloReply, error) {
	return &HelloReply{Message: "Welcome " + req.GetName() + " to " + s.Name + "!"}, nil
}
